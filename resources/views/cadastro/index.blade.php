@extends("cadastro.form.layout")

@section("style")
    <style>
        html, body, .page-wrapper, .page-content-wrapper{
            display:flex;
            justify-content:center;
            align-items:center;
            width:100%;
            height:100%;
        }
        body{
            width:100%;
            height:100%;
        }
        .page-container{
            width:100%;
        }
        .page-content{
            width:100%;
            display:flex !important;
            justify-content:center !important;
            align-items:center !important;
            background-image: url('/img/background.png');
            background-size: auto 70%;
            background-repeat: no-repeat;
            background-position-y: 50%;
        }
        .index{
            color: #ba3c3c;
            font-weight: 600;
        }
        h4{
            color: #206e99;
            font-size: 18px;
        }
        .header-fixed{
            display: none;
        }
    </style>

@endsection

@section("content")
    <form action="{{url("cadastro/form")}}" method="get" style="height:100%;display: flex;justify-content: center;align-items: center">
        <div class="row">
            <div class="col-sm-7 col-sm-offset-3">
                <div class="form-group">
                    <img src="{{asset("/img/logo.png")}}" width="350">
                </div>
                <br>
                <div class="form-group">
                    <h3 class="index">Bem-vindo(a)</h3>
                </div>
                <div class="form-group">
                    <h4>Cadastre-se já para receber pedidos de orçamentos e fechar
                        negócios pelo ServeMe.</h4>
                </div>
                <br>
                <br>
                <div class="form-group">
                    <button type="submit" class="btn btn-primary" style="padding: 6px 35px">Cadastrar</button>
                </div>
            </div>
        </div>
    </form>
@endsection