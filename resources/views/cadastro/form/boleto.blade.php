@extends("cadastro.form.layout")

@section("style")

    <style>
        .btn:active{
            color:#fff;
        }

        .flex-jcsb{
            display:flex;
            justify-content:space-between;
        }

        .btn-none{
            background:transparent;
            outline:none;
            color:#206e99;
            padding:0px;
            font-family:bold;
        }

        .btn-none + .btn-none{
            margin-left:20px !important;
        }
    </style>

@endsection

@section("title")
    <h1 style="color:#206e99; font-weight:bold;">Pagamento com Boleto Bancario</h1>
    <h3 style="color:#206e99;">Primeiro, fale um pouco sobre você</h3>
@endsection

@section("content")
    <div class="row">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="nome">Nome completo</label>
                                <input type="text" id="nome" name="nome" class="form-control">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="cpf">CPF</label>
                                <input type="text" id="cpf" name="cpf" class="form-control cpf">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="email">O boleto será enviado para o seguinte e-mail</label>
                                <input type="email" id="email" name="email" class="form-control">
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label>Vencimento</label><br />
                        <p class="form-control-static">07/10/2017</p>
                    </div>

                    <div class="form-group">
                        <label>Valor</label><br />
                        <p class="form-control-static">189,00</p>
                    </div>

                    <div class="col-md-6" style="margin: 20px 0; padding: 0">
                        <button type="button" class="btn btn-default col-md-4 margin-right-10 btn-cadastro-altura btnCancelarCadastro">Cancelar
                        </button>
                        <button type="button" class="btn btn-warning col-md-4 btn-cadastro-altura" id="btnSalvarRow1">Salvar
                        </button>
                    </div>
                </div>
            </div>

        </div>
    </div>


@endsection

@include("cadastro.modal.servico")