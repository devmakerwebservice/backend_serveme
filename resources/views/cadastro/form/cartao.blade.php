@extends("cadastro.form.layout")

@section("title")
    <h1 style="color:#206e99; font-weight:bold;">Pagamento com Cartão de Crédito</h1>
    <h3 style="color:#206e99;">Primeiro, fale um pouco sobre você</h3>
@endsection

@section("content")

    <div class="row">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="nome">Número do cartão</label>
                                <input type="text" id="nome" name="nome" class="form-control cartao">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="nome">Nome impresso no cartão</label>
                                <input type="text" id="nome" name="nome" class="form-control">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="nome">CPF/CNPJ titular</label>
                                <input type="text" id="nome" name="nome" onblur="verifica_cpf_cnpj($(this).val())" maxlength="19" class="form-control cpf-cnpj">
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="nascimento">CVV</label>
                                <input type="text" id="nascimento" name="nascimento" maxlength="4" class="form-control">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="nome">Validade</label>
                                <input type="text" id="nome" name="nome" maxlength="2" placeholder="Mês" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="nascimento">&nbsp</label>
                                <input type="text" id="nascimento" name="nascimento" maxlength="4" placeholder="Ano" class="form-control">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="nome">Parcelas</label>
                                <input type="text" id="nome" name="nome" class="form-control">
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="nome">1x</label> <br />
                                <p class="form-control-static">300,00</p>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6" style="margin: 20px 0; padding: 0">
                        <button type="button" class="btn btn-default col-md-4 margin-right-10 btn-cadastro-altura btnCancelarCadastro">Cancelar
                        </button>
                        <button type="button" class="btn btn-warning col-md-4 btn-cadastro-altura" id="btnSalvarRow1">Salvar
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection