
<div class="header-fixed">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="row">
                <div class="col-md-12">
                    <img src="{{asset("img/logo.png")}}" width="200">
                </div>
            </div>
            <div class="mt-element-step">
                @if(in_array(\Route::currentRouteName(), ['get.cartao', 'get.boleto']) === false)
                    <div class="row step-line">
                        <div class="col-md-2 mt-step-col first" id="item-1">
                            <div class="mt-step-number step-selected" id="item-1-number">
                            </div>
                            <div class="mt-step-title" id="item-1-title">Dados Básicos</div>
                        </div>
                        <div class="col-md-2 mt-step-col" id="item-2">
                            <div class="mt-step-number" id="item-2-number">
                            </div>
                            <div class="  mt-step-title font-red-cascade" id="item-2-title">Endereço
                            </div>
                        </div>
                        <div class="col-md-2 mt-step-col" id="item-3">
                            <div class="mt-step-number" id="item-3-number">
                            </div>
                            <div class="mt-step-title font-red-cascade" id="item-3-title">Serviços</div>
                        </div>
                        <div class="col-md-2 mt-step-col" id="item-4">
                            <div class="mt-step-number" id="item-4-number">
                            </div>
                            <div class="mt-step-title font-red-cascade" id="item-4-title">Atendimento
                            </div>
                        </div>
                        <div class="col-md-2 mt-step-col" id="item-5">
                            <div class="mt-step-number" id="item-5-number">
                            </div>
                            <div class="mt-step-title font-red-cascade" id="item-5-title">Pagamento</div>
                        </div>
                        <div class="col-md-2 mt-step-col last" id="item-6">
                            <div class="mt-step-number" id="item-6-number">
                            </div>
                            <div class="mt-step-title font-red-cascade" id="item-6-title">Experiência</div>
                        </div>
                    </div>
                @endif
                <div class="row">
                    <div class="col-md-12">
                        @yield("title")
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
