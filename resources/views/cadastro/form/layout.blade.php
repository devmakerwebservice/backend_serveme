<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<html lang="pt-br">
<head>
    <!-- META SECTION -->
    <title>ServeMe</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&amp;subset=all" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="{{asset("/css/bootstrap.min.css")}}">
    <link rel="stylesheet" href="{{asset("/assets/sweetalert2/sweetalert2.css")}}">
    <link rel="stylesheet" href="{{asset("/assets/css/components.min.css")}}">
    <link rel="stylesheet" href="{{asset("/css/app/custom-cadastro.css")}}">
    <link rel="stylesheet" href="{{asset("/css/custom.css")}}">
    <style type="text/css">
        .page-content-wrapper .page-content {
            margin-left: 0px;
            margin-top: 0;
            min-height: 600px;
        }
        .page-header-fixed .page-container {
            margin-top: 0px;
        }
    </style>
    @yield('style')

<body class="page-header-fixed page-sidebar-closed-hide-logo">
<div class="page-wrapper">
    <div class="page-container" style="overflow-x:hidden;">
        <div class="page-sidebar-wrapper">
            <div class="page-sidebar navbar-collapse collapse">
            </div>
        </div>
        <div class="page-content-wrapper">
            <div class="page-content">
                @include('cadastro.form.header')
                @yield('content')
            </div>
        </div>
    </div>
</div>

<script src="{{asset("/js/plugins/jquery.min.js")}}"></script>
<script src="{{asset("/js/plugins/bootstrap/js/bootstrap.min.js")}}"></script>
{{--<script src="{{asset("/js/plugins/icheck/icheck.min.js")}}"></script>--}}

<script src="{{asset("/assets/sweetalert2/sweetalert2.min.js")}}"></script>
<script src="{{asset("/js/app.min.js")}}"></script>
<script src="{{asset("/js/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js")}}"></script>
<script src="{{asset("/js/plugins/jqueryMask/jquery.mask.min.js")}}"></script>
<script src="{{asset("/js/mascaras.js")}}"></script>
<script src="{{asset("/js/mylibs/valida_cpf_cnpj.js")}}"></script>
<script src="{{asset("/js/mylibs/mask_inscricao_estadual.js")}}"></script>
<script src="{{asset("/js/app/cadastro.js")}}"></script>
@yield('script')
</body>

</html>