@extends("cadastro.form.layout")

@section("style")
    <style>
        .image{
            background-image: url('/img/background.png');
            background-size: contain;
            background-repeat: no-repeat;
            height: 52vh;
            /*min-height: 345px;*/
        }
        .azul{
            background-color: #206e99;
            height: 48vh;
            min-height: 305px;
        }
        .btn-primary{
            color: #206e99;
            background-color: #fff;
            font-weight: 600;
        }
        .btn-primary:hover,.btn-primary:focus,.btn-primary:visited {
            background-color: #dcdcdc;
            color: #206e99;
        }
        .preco{
            color: #fff;
            font-size: 36px;
        }
        .mes{
            font-size: 18px;
            color: #fff;
        }
        .desc{
            font-size: 16px;
            color: #fff;
        }

        .btn-none{
            padding:0px;
            background:transparent;
            outline:none;
            border:none;
            font-size:18px;
            color:#206e99;
            font-weight:bold;
            margin-left:20px !important;
        }

        .flex-jce{
            display:flex;
            justify-content:flex-end;
        }

        .header-fixed{
            display: none;
        }

    </style>
@endsection

@section("content")

<div class="row image" style="display: flex;align-items: center;align-content: center">
    <div class="col-sm-7 col-sm-offset-3">
        <div class="form-group">
            <img src="{{asset("/img/logo.png")}}" width="350">
        </div>
        <br>
        <div class="form-group">
            <h3 style="color:#206e99;font-weight: 700">Parabéns!</h3>
        </div>
        <div class="form-group">
            <h4 style="color: #000">Seu cadastro ServeMe foi concluído</h4>
        </div>
        <div class="form-group">
            <p style="font-size: 18px">Falta pouco para você poder receber pedidos de orçamentos que se encaixem no seu perfil e poderá
                enviar quantas propostas quiser. Você não paga nada até fechar o primeiro serviço!
                A partir do segundo serviço, são esses os valores para continuar fechando negócios no ServeMe:</p>
        </div>
    </div>
</div>

<div class="row azul">
    <div class="col-sm-7 col-sm-offset-3">
        <div class="form-group">
            <h2 style="color: #fff">Assinatura Profissional ServeMe</h2>
        </div>
        <div class="form-group">
            <span style="color: #fff">Valores válidos a partir do segundo serviço realizado pelo app*</span>
        </div>
        <div class="form-group">
            <div class="col-md-6">
                <div class="row">
                    <span class="preco">R$ 9,90</span><span class="mes"> ao mês</span>
                </div>
                <div class="row">
                    <span class="desc">Para participar de todos os orçamentos de acordo com o seu cadastro</span>
                </div>
            </div>
            <div class="col-md-6">
                <div>
                    <span class="preco">7%</span><span class="mes"> ao mês</span>
                </div>
                <div class="row">
                    <span class="desc">do valor recebido por cada serviço</span>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-7 col-sm-offset-3">
        <div class="form-group row" style="margin-top: 30px">
            <button type="button" data-toggle="modal" data-target="#pagamento-modal" class="btn btn-primary" style="padding: 6px 35px">Cadastrar</button>
        </div>
    </div>
</div>
@endsection

@include("cadastro.modal.pagamento")