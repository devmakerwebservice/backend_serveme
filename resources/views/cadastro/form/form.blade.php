@extends("cadastro.form.layout")

@section("style")
    <style>
        .flex-jcsb{
            display:flex;
            justify-content:space-between;
        }

        .btn-none{
            background:transparent;
            outline:none;
            color:#206e99;
            padding:0px;
            font-family:bold;
        }

        .btn-none + .btn-none{
            margin-left:20px !important;
        }

        .btn.btn-default.outline{
            background:transparent;
            outline:none;
            border:1px solid #206e99;
            color:#206e99;
        }

        .subType-payment{
            padding: 0 15px;
        }
    </style>
@endsection

@section("content")
<div class="row">
    <div class="col-md-12">
        <form method="POST" id="form1">
            <div class="row" id="divRowPrimeiraEtapa" style="display: none">
                <div class="col-md-6 col-md-offset-3">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="form-group">
                                <label for="nome">Qual o seu nome completo?</label>
                                <input type="text" id="nome" name="nome" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="data_nascimento">Data de nascimento</label>
                                <input type="text" id="data_nascimento" name="data_nascimento" class="form-control dataCalendario">
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <label for="email">E-mail</label>
                                <input type="email" id="email" name="email" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="tel">Telefone</label>
                                <input type="text" id="tel" name="telefone" class="form-control telefone">
                            </div>
                        </div>
                    </div>
                    <div class="row text-center">
                        <div class="col-md-6">
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="btn-group btn-block" role="group">
                            <button type="button" class="btn pessoa" id="pessoa1" onclick="userTipo(1)">Pessoa Física</button>
                            <button type="button" class="btn pessoa" id="pessoa2" onclick="userTipo(2)">Pessoa Jurídica</button>
                        </div>
                    </div>

                    <div id="documentosPessoaFisica">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="cpf">Inform seu CPF</label>
                                    <input type="text" id="cpf" name="cpf" onblur="verifica_cpf_cnpj(this.value)" class="form-control cpf">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="perfil_nome">Nome para seu perfil profissional</label>
                                    <input type="text" id="perfil_nome" name="perfil_nome" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="site_facebook">Site ou Facebook (opcional)</label>
                                    <input type="url" id="site_facebook" name="site_facebook" class="form-control">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="documentosPessoaJuridica">
                        <div class="row">
                            <div class="col-md-5">
                                <div class="form-group">
                                    <label for="cnpj">Informe seu CNPJ</label>
                                    <input type="text" id="cnpj" name="cnpj" onblur="verifica_cpf_cnpj(this.value)" class="form-control cnpj">
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="uf">UF</label>
                                    <select class="form-control" id="uf_inscricao_estadual">
                                        <option selected disabled value=""></option>
                                        <option value="AC">AC</option>
                                        <option value="AL">AL</option>
                                        <option value="AM">AM</option>
                                        <option value="AP">AP</option>
                                        <option value="BA">BA</option>
                                        <option value="CE">CE</option>
                                        <option value="DF">DF</option>
                                        <option value="ES">ES</option>
                                        <option value="GO">GO</option>
                                        <option value="MA">MA</option>
                                        <option value="MG">MG</option>
                                        <option value="MS">MS</option>
                                        <option value="MT">MT</option>
                                        <option value="PA">PA</option>
                                        <option value="PB">PB</option>
                                        <option value="PE">PE</option>
                                        <option value="PI">PI</option>
                                        <option value="PR">PR</option>
                                        <option value="RJ">RJ</option>
                                        <option value="RN">RN</option>
                                        <option value="RO">RO</option>
                                        <option value="RR">RR</option>
                                        <option value="RS">RS</option>
                                        <option value="SC">SC</option>
                                        <option value="SE">SE</option>
                                        <option value="SP">SP</option>
                                        <option value="TO">TO</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-5">
                                <div class="form-group">
                                    <label for="inscricao_estadual">Inscrição Estadual (opcional)</label>
                                    <input type="text" disabled id="inscricao_estadual" name="inscricao_estadual" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="tel2">Telefone 2 (opcional)</label>
                                    <input type="text" id="tel2" name="tel2" class="form-control telefone">
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="form-group">
                                    <label for="razao">Razão social</label>
                                    <input type="text" id="razao" name="razao" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="form-group">
                                    <label for="fantasia_nome">Nome fantasia (será exibido no perfil profissional)</label>
                                    <input type="text" id="fantasia_nome" name="fantasia_nome" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="form-group">
                                    <label for="site_facebook">Site ou Facebook</label>
                                    <input type="url" id="site_facebook" name="site_facebook" class="form-control">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6" style="margin: 20px 0; padding: 0">
                        <button type="button" class="btn btn-default col-md-4 margin-right-10 btn-cadastro-altura btnCancelarCadastro">Cancelar
                        </button>
                        <button type="button" class="btn btn-warning col-md-4 btn-cadastro-altura" id="btnSalvarRow1">Salvar
                        </button>
                    </div>
                </div>
            </div>

            <div class="row" id="divRowSegundaEtapa" style="display: none">
                <div class="col-md-6 col-md-offset-3">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="cep">Informe seu CEP</label>
                                <input type="text" id="cep" name="cep" class="form-control cep">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-9">
                            <div class="form-group">
                                <label for="logra">Logradouro</label>
                                <input type="text" id="rua" name="rua" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="num">Número</label>
                                <input type="text" id="num" name="numero" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-7">
                            <div class="form-group">
                                <label for="complemento">Complemento</label>
                                <input type="text" id="complemento" name="complemento" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-5">
                            <div class="form-group">
                                <label for="bairro">Bairro</label>
                                <input type="text" id="bairro" name="bairro" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="cidade">Cidade</label>
                                <input type="text" id="cidade" name="cidade" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="estado">Estado</label>
                                <input type="text" id="estado" name="estado" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6" style="margin: 20px 0; padding: 0">
                        <button type="button" class="btn btn-default col-md-4 margin-right-10 btn-cadastro-altura btnCancelarCadastro">Cancelar
                        </button>
                        <button type="button" class="btn btn-warning col-md-4 btn-cadastro-altura" id="btnSalvarRow2">Salvar
                        </button>
                    </div>
                </div>
            </div>

            <div class="row" id="divRowTerceiraEtapa" style="display: none">
                <div class="col-md-6 col-md-offset-3">

                    <div class="col-md-6">

                        <?php

                        $servicos1 = [
                            'assistencia-tecnica' => 'Assistência Técnica',
                            'aulas' => 'Aulas',
                            'casa-escritorio' => 'Casa e Escritório',
                            'construcao-reforma' => 'Construção e Reforma',
                            'consultoria' => 'Consultoria'
                        ];

                        ?>

                        <?php foreach ($servicos1 as $key => $value){ ?>
                        <div class="form-group">
                            <div class="custom-panel">

                                <div class="servico" data-toggle="modal" data-target="#servicos-modal">
                                    <h4><?php echo $value; ?></h4>
                                </div>

                                <div class="servicos">

                                </div>

                            </div>
                        </div>
                        <?php } ?>

                    </div>

                    <div class="col-md-6">

                        <?php

                        $servicos2 = [
                            'design-tecnologia' => 'Design e Tecnologia',
                            'eventos' => 'Eventos',
                            'moda-beleza' => 'Moda e Beleza',
                            'saude' => 'Saúde',
                            'veiculos' => 'Veículos'
                        ];

                        ?>

                        <?php foreach ($servicos2 as $key => $value){ ?>
                        <div class="form-group">
                            <div class="custom-panel open">

                                <div class="servico" data-toggle="modal" data-target="#servicos-modal">
                                    <h4><?php echo $value; ?></h4>
                                </div>

                                <div class="servicos">

                                    <p>
                                        Cerca Elétrica e Portão Eletrônico
                                        Desentupimento de Fossa
                                        Impermeabilização de Telhado
                                    </p>

                                </div>

                            </div>
                        </div>
                        <?php } ?>

                    </div>

                    <div class="col-md-6" style="margin: 20px 0; padding: 0">
                        <button type="button" class="btn btn-default col-md-4 margin-right-10 btn-cadastro-altura btnCancelarCadastro">Cancelar
                        </button>
                        <button type="button" class="btn btn-warning col-md-4 btn-cadastro-altura" id="btnSalvarRow3">Salvar
                        </button>
                    </div>
                </div>
            </div>

            <div class="row" id="divRowQuartaEtapa" style="display: none">
                <div class="col-md-8 col-md-offset-2">
                    <div class="row">
                        <div class="col-md-12" id="turnos">

                        </div>
                        <hr>
                    </div>

                    <div class="row" style="margin-top: 20px; padding: 0">
                        <div class="col-md-3">
                            <div class="form-group">
                                <a href="#" onclick="AddTurno()" class="link-servico">
                                    <div class="servico">
                                        Adicionar turno
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6" style="margin: 20px 0; padding: 0">
                        <button type="button" class="btn btn-default col-md-4 margin-right-10 btn-cadastro-altura btnCancelarCadastro">Cancelar
                        </button>
                        <button type="button" class="btn btn-warning col-md-4 btn-cadastro-altura" id="btnSalvarRow4">Salvar
                        </button>
                    </div>
                </div>
            </div>

            <div class="row" id="divRowQuintaEtapa" style="display: none">
                <div class="col-md-6 col-md-offset-3">
                    <div class="col-md-12">
                        <div class="form-group">
                            <h4>Selecione</h4>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="checkbox-content">
                                        <input type="radio" class="checkbox primary" id="tipo-1" name="tipo" value="Dinheiro" checked/>
                                        <label for="tipo-1">
                                            <span>Dinheiro</span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="checkbox-content">
                                        <input type="radio" class="checkbox primary" id="tipo-2" name="tipo" value="Transferência bancária"/>
                                        <label for="tipo-2">
                                            <span>Transferência bancária</span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="checkbox-content">
                                        <input type="radio" class="checkbox primary" id="tipo-3" name="tipo" value="Cartão de Débito"/>
                                        <label for="tipo-3">
                                            <span>Cartão de Débito</span>
                                        </label>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="subType-payment">
                                        <div class="col-md-2">
                                            <div class="checkbox-content">
                                                <input type="checkbox" class="checkbox primary" id="deb-1" name="debito[]" value="Elo"/>
                                                <label for="deb-1">
                                                    <span>Elo</span>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="checkbox-content">
                                                <input type="checkbox" class="checkbox primary" id="deb-2" name="debito[]" value="Master"/>
                                                <label for="deb-2">
                                                    <span>Master</span>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="checkbox-content">
                                                <input type="checkbox" class="checkbox primary" id="deb-3" name="debito[]" value="Visa"/>
                                                <label for="deb-3">
                                                    <span>Visa</span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="checkbox-content">
                                        <input type="radio" class="checkbox primary" id="tipo-4" name="tipo" value="Cartão de Crédito"/>
                                        <label for="tipo-4">
                                            <span>Cartão de Crédito</span>
                                        </label>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="subType-payment">
                                        <div class="col-md-2">
                                            <div class="checkbox-content">
                                                <input type="checkbox" class="checkbox primary" id="cred-1" name="credito[]" value="Elo"/>
                                                <label for="cred-1">
                                                    <span>Elo</span>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="checkbox-content">
                                                <input type="checkbox" class="checkbox primary" id="cred-2" name="credito[]" value="Master"/>
                                                <label for="cred-2">
                                                    <span>Master</span>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="checkbox-content">
                                                <input type="checkbox" class="checkbox primary" id="cred-3" name="credito[]" value="Visa"/>
                                                <label for="cred-3">
                                                    <span>Visa</span>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="checkbox-content">
                                                <input type="checkbox" class="checkbox primary" id="cred-3" name="credito[]" value="American"/>
                                                <label for="cred-3">
                                                    <span>American Express</span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6" style="margin: 20px 0; padding: 0">
                        <button type="button" class="btn btn-default col-md-4 margin-right-10 btn-cadastro-altura btnCancelarCadastro">Cancelar
                        </button>
                        <button type="button" class="btn btn-warning col-md-4 btn-cadastro-altura" id="btnSalvarRow5">Salvar
                        </button>
                    </div>
                </div>
            </div>

            <div class="row" id="divRowSextaEtapa" style="display: none">
                <div class="col-md-6 col-md-offset-3">
                    <div class="col-md-12 experiencias">

                        <h3 style="color:#206e99;">
                            Precisamos do contato de 3 pessoas que já tenham contratado seus
                            serviços. Elas poderão dar referências sobre o seu trabalho
                        </h3>

                        <br />

                        <h5 style="color:#206e99;">Contato 1</h5>

                        <div class="row">
                            <div class="col-md-8">
                                <div class="form-group">
                                    <label for="nome1">Nome completo</label>
                                    <input type="text" id="nome1" name="contato[1][nome]" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="telefone1">Telefone</label>
                                    <input type="text" id="telefone1" name="contato[1][telefone]" class="form-control telefone" >
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="email1">E-mail</label>
                                    <input type="email" id="email1" name="contato[1][email]" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="form-group">
                                    <label for="servico1">Serviço realizado</label>
                                    <input type="text" id="servico1" name="contato[1][servico]" class="form-control">
                                </div>
                            </div>
                        </div>

                        <h5 style="color:#206e99;">Contato 2</h5>

                        <div class="row">
                            <div class="col-md-8">
                                <div class="form-group">
                                    <label for="nome2">Nome completo</label>
                                    <input type="text" id="nome2" name="contato[2][nome]" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="telefone2">Telefone</label>
                                    <input type="text" id="telefone2" name="contato[2][telefone]" class="form-control telefone">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="email2">E-mail</label>
                                    <input type="email" id="email2" name="contato[2][email]" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="form-group">
                                    <label for="servico2">Serviço realizado</label>
                                    <input type="text" id="servico2" name="contato[2][servico]" class="form-control">
                                </div>
                            </div>
                        </div>

                        <h5 style="color:#206e99;">Contato 3</h5>

                        <div class="row">
                            <div class="col-md-8">
                                <div class="form-group">
                                    <label for="nome3">Nome completo</label>
                                    <input type="text" id="nome3" name="contato[3][nome]" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="telefone3">Telefone</label>
                                    <input type="text" id="telefone3" name="contato[3][telefone]" class="form-control telefone">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="email3">E-mail</label>
                                    <input type="text" id="email3" name="contato[3][email]" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="form-group">
                                    <label for="servico3">Serviço realizado</label>
                                    <input type="text" id="servico3" name="contato[3][servico]" class="form-control">
                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="col-md-6" style="margin: 20px 0; padding: 0">
                        <button type="button" class="btn btn-default col-md-4 margin-right-10 btn-cadastro-altura btnCancelarCadastro">Cancelar
                        </button>
                        <button type="button" class="btn btn-warning col-md-4 btn-cadastro-altura" id="btnSalvarRow6">Salvar
                        </button>
                    </div>
                </div>
            </div>

            <div class="row" id="divRowSetimaEtapa" style="display: none">
                <div class="col-md-6 col-md-offset-3">
                    <div class="col-md-12 confere">
                        <div class="row">
                            <h4 style="color:#206e99;">Dados Básicos</h4>
                            <div class="col-md-6">
                                <h4>Nome completo</h4>
                                <span>Maria José da Silveira</span>
                            </div>

                            <div class="col-md-6">
                                <h4>Data de Nascimento</h4>
                                <span>12/12/1980</span>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <h4>CNPJ</h4>
                                <span>222.222.222/0001-01</span>
                            </div>

                            <div class="col-md-6">
                                <h4>Inscrição Estadual</h4>
                                <span>-</span>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <h4>Razão social</h4>
                                <span>Nome da Minha Empresa</span>
                            </div>

                            <div class="col-md-6">
                                <h4>Nome Fantasia</h4>
                                <span>Minha Empresa</span>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <h4>Telefone</h4>
                                <span>(41) 3333-3333</span>
                            </div>

                            <div class="col-md-6">
                                <h4>E-mail</h4>
                                <span>contato@gmail.com</span>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <h4>Site/Facebook</h4>
                                <span>www.empresa.com.br</span>
                            </div>
                        </div>

                        <button class="btn btn-default outline" onclick="carregaEtapa(1)">CORRIGIR</button>

                        <hr />

                        <h4 style="color:#206e99;">Endereço</h4>

                        <div class="row">
                            <div class="col-md-6">
                                <h4>CEP</h4>
                                <span>99999-999</span>
                            </div>

                            <div class="col-md-6">
                                <h4>Rua/Avenida</h4>
                                <span>Al. Dr. Carlos de Carvalho</span>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-4">
                                <h4>Nº</h4>
                                <span>771</span>
                            </div>

                            <div class="col-md-4">
                                <h4>Complemento</h4>
                                <span>7º andar</span>
                            </div>

                            <div class="col-md-4">
                                <h4>Bairro</h4>
                                <span>Centro</span>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <h4>Cidade</h4>
                                <span>Curitiba</span>
                            </div>

                            <div class="col-md-6">
                                <h4>Estado</h4>
                                <span>Paraná</span>
                            </div>
                        </div>

                        <button class="btn btn-default outline" onclick="carregaEtapa(2)">CORRIGIR</button>

                        <hr />

                        <h4 style="color:#206e99;">Serviços</h4>

                        <div class="row">
                            <div class="col-md-6">
                                <h4>Casa e Construção</h4>
                                <span>Cerca Elétrica e Portão Eletrônico</span>
                                <span>Desentupimento de Fossa</span>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <h4>Design e Tecnologia</h4>
                                <span>Design Gráfico</span>
                                <span>Web Design</span>
                            </div>
                        </div>

                        <button class="btn btn-default outline" onclick="carregaEtapa(3)">CORRIGIR</button>

                        <hr />

                        <h4 style="color:#206e99;">Atendimento</h4>

                        <div class="row">
                            <div class="col-md-6">
                                <h4>Segunda</h4>
                                <span>8:00 - 10:00</span>
                            </div>

                            <div class="col-md-6">
                                <h4>Terça</h4>
                                <span>8:00 - 10:00</span>
                            </div>
                        </div>

                        <button class="btn btn-default outline" onclick="carregaEtapa(4)">CORRIGIR</button>

                        <hr />

                        <h4 style="color:#206e99;">Pagamento</h4>

                        <div class="row">
                            <div class="col-md-6">
                                <h4>Dinheiro</h4>
                                <span>&nbsp</span>
                            </div>

                            <div class="col-md-6">
                                <h4>Cartão de Crédito</h4>
                                <span>Visa, MasterCard, Elo</span>
                            </div>
                        </div>

                        <button class="btn btn-default outline" onclick="carregaEtapa(5)">CORRIGIR</button>

                        <hr />

                        <h4 style="color:#206e99;">Experiência</h4>

                        <div class="row">
                            <div class="col-md-12">
                                <h4>Comentário</h4>
                                <span>Pintura texturizada realziada no ano pasado</span>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <h4>Arquivos</h4>
                                <span>Arquivo1.jpeg</span>
                                <span>Arquivo2.jpeg</span>
                            </div>
                        </div>

                        <h4>Contatos</h4>

                        <div class="row">
                            <div class="col-md-6">

                                <div class="form-group">
                                    <h4>Nome contato 1</h4>
                                    <span>(41) 9 9872-7268</span>
                                </div>

                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <h4>Nome Serviço realizado</h4>
                                    <span>contato@gmail.com</span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6" style="margin: 20px 0; padding: 0">
                        <button type="button" class="btn btn-default col-md-4 margin-right-10 btn-cadastro-altura btnCancelarCadastro">Cancelar
                        </button>
                        <button type="submit" class="btn btn-warning col-md-4 btn-cadastro-altura">Enviar
                        </button>
                    </div>
                </div>
            </div>

        </form>

    </div>
</div>
@endsection

@include("cadastro.modal.servico")

