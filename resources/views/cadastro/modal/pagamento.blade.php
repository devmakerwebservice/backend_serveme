<div class="modal type-01" id="pagamento-modal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <h4 style="color:#206e99;">Selecione uma forma de pagamento</h4>
                <div class="radio-content">
                    <input type="radio" class="radio primary" value="{{url("cadastro/pagamento/boleto")}}" name="pagamento" id="pagamento-1" />
                    <label for="pagamento-1">
                        <div class="checked"></div>
                        <span>Boleto</span>
                    </label>

                    <input type="radio" class="radio primary" value="{{url("cadastro/pagamento/cartao")}}" name="pagamento" id="pagamento-2" />
                    <label for="pagamento-2">
                        <div class="checked"></div>
                        <span>Cartão de crédito</span>
                    </label>
                </div>

                <div class="flex-jce">
                    <button type="button" class="btn btn-none">Cancelar</button>
                    <button type="button" class="btn btn-none forma-pagamento">Selecionar</button>
                </div>
            </div>
        </div>
    </div>
</div>