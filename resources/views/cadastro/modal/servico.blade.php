<div class="modal type-01" id="servicos-modal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <h4 style="color:#206e99;">Selecione os serviços que você oferece</h4>
                <div class="row">
                    <div class="col-md-6">
                        <?php for($i=1; $i< 10; $i++){ ?>
                        <div class="checkbox-content">
                            <input type="checkbox" class="checkbox primary" id="checkbox-<?php echo $i; ?>" />
                            <label for="checkbox-<?php echo $i; ?>">
                                <span>Checkbox 1</span>
                            </label>
                        </div>
                        <?php } ?>
                    </div>

                    <div class="col-md-6">
                        <?php for($i=1; $i< 10; $i++){ ?>
                        <div class="checkbox-content">
                            <input type="checkbox" class="checkbox primary" id="checkbox-<?php echo $i; ?>" />
                            <label for="checkbox-<?php echo $i; ?>">
                                <span>Checkbox 1</span>
                            </label>
                        </div>
                        <?php } ?>
                    </div>
                </div>

                <div class="flex-jcsb">
                    <div class="left">
                        <button class="btn btn-none">CANCELAR</button>
                    </div>

                    <div class="right">
                        <button class="btn btn-none">SELECIONAR TUDO</button>
                        <button class="btn btn-none">SELECIONAR</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>