<?php

namespace App\Http\Controllers\Cadastro;

use App\Services\PrestadorService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CadastroController extends Controller
{
    /**
     * @var PrestadorService
     */
    private $service;

    /**
     * AnimalController constructor.
     * @param PrestadorService $service
     */
    public function __construct(PrestadorService $service)
    {
        $this->service = $service;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view("cadastro.index");
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("cadastro.form.form");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();

        $reponse = array();

        $reponse['Success'] = true;
        $reponse['Mensagem'] = "erro";
        $reponse['Dataaa'] = $data;

        return $reponse;
    }

    /**
     * Display the data confirm view.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function finish()
    {
        return view('cadastro.form.finish');
    }

    /**
     * Display the payment way Boleto view.
     *
     * @return \Illuminate\Http\Response
     */
    public function boleto()
    {
        return view('cadastro.form.boleto');
    }

    /**
     * Display the payment way Boleto view.
     *
     * @return \Illuminate\Http\Response
     */
    public function cartao()
    {
        return view('cadastro.form.cartao');
    }

    public function validatorSteps(Request $request, $step)
    {
        $data = $request->all();

        switch ($step) {
            case "1":
                return $this->service->validator_dados_basicos($data);
            break;
            case "2":
                return $this->service->validator_endereco($data);
            break;
            case "3":
                return $this->service->validator_servicos($data);
            break;
            case "4":
                return $this->service->validator_atendimento($data);
            break;
            case "5":
                return $this->service->validator_pagamento($data);
            break;
            case "6":
                return $this->service->validator_experiencia($data);
            break;
        }
    }
}
