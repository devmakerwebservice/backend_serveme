<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix' => 'cadastro'], function () {
    Route::get('/', 'Cadastro\\CadastroController@index');
    Route::get('/form', 'Cadastro\\CadastroController@create');
    Route::post('/form', 'Cadastro\\CadastroController@store');
    Route::get('/check', 'Cadastro\\CadastroController@confere');
    Route::get('/finish', 'Cadastro\\CadastroController@finish');
    Route::get('/validacao/{step}', 'Cadastro\\CadastroController@finish');
    Route::group(['prefix' => 'pagamento'], function (){
        Route::get('/boleto', 'Cadastro\\CadastroController@boleto')->name('get.boleto');
        Route::post('/boleto', 'Cadastro\\CadastroController@boleto')->name('post.boleto');
        Route::get('/cartao', 'Cadastro\\CadastroController@cartao')->name('get.cartao');
        Route::post('/cartao', 'Cadastro\\CadastroController@cartao')->name('post.cartao');
    });
});