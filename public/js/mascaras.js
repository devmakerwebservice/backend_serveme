$(function() {
    $(".telefone").mask("(99) 9999-9999", {reverse:true, placeholder: '(00) 0000-0000'});
    $(".porcentagem").mask('000,00%', {reverse: true});
    $(".decimal").mask('##0000,00');
    $(".cep").mask('00000-000', {reverse: true, placeholder: '00000-000'});
    $(".dinheiro").mask('000.000.000.000.000,00', {reverse: true});
    $(".dataCalendario").mask('99/99/9999', {placeholder: '00/00/0000'});
    $(".hora").mask('99:99', {placeholder: '00:00'});
    $('.peso').mask("#0.000", {reverse: true});
    $('.cpf').mask('000.000.000-00', {reverse: true, placeholder: '000.000.000-00'});
    $('.cnpj').mask('00.000.000/0000-00', {reverse: true, placeholder: '00.000.000/0000-00'});
    $('.cartao').mask('9999 9999 9999 9999', {placeholder: '0000 0000 0000 0000'});

    $(".cpf-cnpj").focusin(function(){
        $(this).unmask();
    });
    $(".cpf-cnpj").focusout(function(){
        if ($(this).val().length == 11){
            $(this).mask('000.000.000-00', {reverse:true});
        }
        else if ($(this).val().length >= 14){
            $(this).mask('00.000.000/0000-00', {reverse:true});
        }
    });

    $(".telefone").focusin(function(){
        $(this).unmask();
    });
    $(".telefone").focusout(function(){
        if ($(this).val().length == 10){
            $(this).mask("(99) 9999-9999", {reverse:true, placeholder: '(00) 0000-0000'});
        }
        else if ($(this).val().length >= 11){
            $(this).mask("(99) 9 9999-9999", {reverse:true, placeholder: '(00) 0 0000-0000'});
        }
    });
});