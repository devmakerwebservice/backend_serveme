var numeroTurno = 1;
$(document).ready(function(){

    $('.banco').click(function(){
        $('.banco').removeClass('active')
        $(this).toggleClass('active');
    });

    carregaEtapa(1);
    AddTurno();
    userTipo(1);

    $(".options").each(function( index ) {
        $(this).parent().css("display", "none");
    });

    $('#btnSalvarRow1').on('click', function () {
        carregaEtapa(2);
    });

    $('#btnSalvarRow2').on('click', function () {
        carregaEtapa(3);
    });

    $('#btnSalvarRow3').on('click', function () {
        carregaEtapa(4);
    });

    $('#btnSalvarRow4').on('click', function () {
        carregaEtapa(5);
    });

    $('#btnSalvarRow5').on('click', function () {
        carregaEtapa(6);
    });

    $('#btnSalvarRow6').on('click', function () {
        carregaEtapa(7);
    });

    $('#btnSalvarRow7').on('click', function () {
        alert("teste");
    });

    $('.btnCancelarCadastro').on('click', function () {
        swal({
            title: "Tem certeza?",
            text: "Tem certeza que deseja cancelar o cadastro?",
            type: "error",
            showCancelButton: true,
            cancelButtonText: "Não",
            confirmButtonColor: "#206e99",
            confirmButtonText: "Sim, tenho certeza!",
            closeOnConfirm: true,
            allowOutsideClick: true,
            showLoaderOnConfirm: false
        }, function () {
            window.location = '/cadastro';
        });
        return false;
    });

    $('#form1').submit(function () {
        $.ajax({
            type: 'POST',
            url: '/cadastro/form',
            data: $("#form1 :input").serializeArray(),
            success: function(data){
            }
        })
        .done(function (data) {
            var retorno = eval(data);
            if(!retorno.Success){
                swal(retorno.Mensagem);
            } else {
                carregaEtapa(7)
            }
        });
       return false;
    });

    $("#cep").blur(function () {
        var cep = $(this).val().replace(/\D/g, '');
        var aux = 0;
        if (cep != "") {

            $.getJSON("//viacep.com.br/wss/" + cep + "/json/?callback=?", function (dados) {
                if (!("erro" in dados)) {
                    console.log("viacep");
                    aux =1;
                    $("#rua").val(dados.logradouro);
                    $("#bairro").val(dados.bairro);
                    $("#cidade").val(dados.localidade);
                    $("#estado").val(dados.uf);
                }
            });
            if(aux==0){
                $.getJSON("http://apps.widenet.com.br/busca-cep/api/cep.json?code=" + cep, function (dados) {
                    if (!("erro" in dados)) {
                        expr = "-";
                        var verifica = dados.address.search(expr);
                        if(verifica>0){
                            var remover = dados.address.substr(verifica, 22, -1);
                            var endereco = dados.address.replace(remover, "");
                            $("#rua").val(endereco);
                        }else{
                            $("#rua").val(dados.address);
                        }
                        $("#bairro").val(dados.district);
                        $("#cidade").val(dados.city);
                        $("#estado").val(dados.state);
                    }
                });
            }
        }
    });

    $('.forma-pagamento').on('click', function () {
        window.location = $('.radio:checked').val();
    })

});

function carregaEtapa(proximaEtapa) {

    if (proximaEtapa === 1){
        $("#item-1").removeClass("done");
        $("#item-1-number").addClass("step-selected");
        $("#item-1-number").removeClass("bg-done");

        $("#item-2").removeClass("done");
        $("#item-2-number").removeClass("step-selected");
        $("#item-2-number").removeClass("bg-done");

        $("#item-3").removeClass("done");
        $("#item-3-number").removeClass("step-selected");
        $("#item-3-number").removeClass("bg-done");

        $("#item-4").removeClass("done");
        $("#item-4-number").removeClass("step-selected");
        $("#item-4-number").removeClass("bg-done");

        $("#item-5").removeClass("done");
        $("#item-5-number").removeClass("step-selected");
        $("#item-5-number").removeClass("bg-done");

        $("#item-6").removeClass("done");
        $("#item-6-number").removeClass("step-selected");
        $("#item-6-number").removeClass("bg-done");

        $("#divRowPrimeiraEtapa").show();
        $("#divRowSegundaEtapa").hide();
        $("#divRowTerceiraEtapa").hide();
        $("#divRowQuartaEtapa").hide();
        $("#divRowQuintaEtapa").hide();
        $("#divRowSextaEtapa").hide();
        $("#divRowSetimaEtapa").hide();

    }else if (proximaEtapa === 2){
        $("#item-1").addClass("done");
        $("#item-1-number").removeClass("step-selected");
        $("#item-1-number").addClass("bg-done");

        $("#item-2").removeClass("done");
        $("#item-2-number").addClass("step-selected");
        $("#item-2-number").removeClass("bg-done");

        $("#item-3").removeClass("done");
        $("#item-3-number").removeClass("step-selected");
        $("#item-3-number").removeClass("bg-done");

        $("#item-4").removeClass("done");
        $("#item-4-number").removeClass("step-selected");
        $("#item-4-number").removeClass("bg-done");

        $("#item-5").removeClass("done");
        $("#item-5-number").removeClass("step-selected");
        $("#item-5-number").removeClass("bg-done");

        $("#item-6").removeClass("done");
        $("#item-6-number").removeClass("step-selected");
        $("#item-6-number").removeClass("bg-done");

        $("#divRowPrimeiraEtapa").hide();
        $("#divRowSegundaEtapa").show();
        $("#divRowTerceiraEtapa").hide();
        $("#divRowQuartaEtapa").hide();
        $("#divRowQuintaEtapa").hide();
        $("#divRowSextaEtapa").hide();
        $("#divRowSetimaEtapa").hide();

    }else if (proximaEtapa === 3){
        $("#item-1").addClass("done");
        $("#item-1-number").removeClass("step-selected");
        $("#item-1-number").addClass("bg-done");

        $("#item-2").addClass("done");
        $("#item-2-number").removeClass("step-selected");
        $("#item-2-number").addClass("bg-done");

        $("#item-3").removeClass("done");
        $("#item-3-number").addClass("step-selected");
        $("#item-3-number").removeClass("bg-done");

        $("#item-4").removeClass("done");
        $("#item-4-number").removeClass("step-selected");
        $("#item-4-number").removeClass("bg-done");


        $("#item-5").removeClass("done");
        $("#item-5-number").removeClass("step-selected");
        $("#item-5-number").removeClass("bg-done");

        $("#item-6").removeClass("done");
        $("#item-6-number").removeClass("step-selected");
        $("#item-6-number").removeClass("bg-done");

        $("#divRowPrimeiraEtapa").hide();
        $("#divRowSegundaEtapa").hide();
        $("#divRowTerceiraEtapa").show();
        $("#divRowQuartaEtapa").hide();
        $("#divRowQuintaEtapa").hide();
        $("#divRowSextaEtapa").hide();
        $("#divRowSetimaEtapa").hide();

    }else if (proximaEtapa === 4){
        $("#item-1").addClass("done");
        $("#item-1-number").removeClass("step-selected");
        $("#item-1-number").addClass("bg-done");

        $("#item-2").addClass("done");
        $("#item-2-number").removeClass("step-selected");
        $("#item-2-number").addClass("bg-done");

        $("#item-3").addClass("done");
        $("#item-3-number").removeClass("step-selected");
        $("#item-3-number").addClass("bg-done");

        $("#item-4").removeClass("done");
        $("#item-4-number").addClass("step-selected");
        $("#item-4-number").removeClass("bg-done");


        $("#item-5").removeClass("done");
        $("#item-5-number").removeClass("step-selected");
        $("#item-5-number").removeClass("bg-done");

        $("#item-6").removeClass("done");
        $("#item-6-number").removeClass("step-selected");
        $("#item-6-number").removeClass("bg-done");

        $("#divRowPrimeiraEtapa").hide();
        $("#divRowSegundaEtapa").hide();
        $("#divRowTerceiraEtapa").hide();
        $("#divRowQuartaEtapa").show();
        $("#divRowQuintaEtapa").hide();
        $("#divRowSextaEtapa").hide();
        $("#divRowSetimaEtapa").hide();

    }else if (proximaEtapa === 5){
        $("#item-1").addClass("done");
        $("#item-1-number").removeClass("step-selected");
        $("#item-1-number").addClass("bg-done");

        $("#item-2").addClass("done");
        $("#item-2-number").removeClass("step-selected");
        $("#item-2-number").addClass("bg-done");

        $("#item-3").addClass("done");
        $("#item-3-number").removeClass("step-selected");
        $("#item-3-number").addClass("bg-done");

        $("#item-4").addClass("done");
        $("#item-4-number").removeClass("step-selected");
        $("#item-4-number").addClass("bg-done");

        $("#item-5").removeClass("done");
        $("#item-5-number").addClass("step-selected");
        $("#item-5-number").removeClass("bg-done");

        $("#item-6").removeClass("done");
        $("#item-6-number").removeClass("step-selected");
        $("#item-6-number").removeClass("bg-done");

        $("#divRowPrimeiraEtapa").hide();
        $("#divRowSegundaEtapa").hide();
        $("#divRowTerceiraEtapa").hide();
        $("#divRowQuartaEtapa").hide();
        $("#divRowQuintaEtapa").show();
        $("#divRowSextaEtapa").hide();
        $("#divRowSetimaEtapa").hide();

    }else if (proximaEtapa === 6){
        $("#item-1").addClass("done");
        $("#item-1-number").removeClass("step-selected");
        $("#item-1-number").addClass("bg-done");

        $("#item-2").addClass("done");
        $("#item-2-number").removeClass("step-selected");
        $("#item-2-number").addClass("bg-done");

        $("#item-3").addClass("done");
        $("#item-3-number").removeClass("step-selected");
        $("#item-3-number").addClass("bg-done");

        $("#item-4").addClass("done");
        $("#item-4-number").removeClass("step-selected");
        $("#item-4-number").addClass("bg-done");

        $("#item-5").addClass("done");
        $("#item-5-number").removeClass("step-selected");
        $("#item-5-number").addClass("bg-done");

        $("#item-6").removeClass("done");
        $("#item-6-number").addClass("step-selected");
        $("#item-6-number").removeClass("bg-done");

        $("#divRowPrimeiraEtapa").hide();
        $("#divRowSegundaEtapa").hide();
        $("#divRowTerceiraEtapa").hide();
        $("#divRowQuartaEtapa").hide();
        $("#divRowQuintaEtapa").hide();
        $("#divRowSextaEtapa").show();
        $("#divRowSetimaEtapa").hide();
    }else if (proximaEtapa === 7){
        $("#item-1").addClass("done");
        $("#item-1-number").removeClass("step-selected");
        $("#item-1-number").addClass("bg-done");

        $("#item-2").addClass("done");
        $("#item-2-number").removeClass("step-selected");
        $("#item-2-number").addClass("bg-done");

        $("#item-3").addClass("done");
        $("#item-3-number").removeClass("step-selected");
        $("#item-3-number").addClass("bg-done");

        $("#item-4").addClass("done");
        $("#item-4-number").removeClass("step-selected");
        $("#item-4-number").addClass("bg-done");

        $("#item-5").addClass("done");
        $("#item-5-number").removeClass("step-selected");
        $("#item-5-number").addClass("bg-done");

        $("#item-6").addClass("done");
        $("#item-6-number").removeClass("step-selected");
        $("#item-6-number").addClass("bg-done");

        $("#divRowPrimeiraEtapa").hide();
        $("#divRowSegundaEtapa").hide();
        $("#divRowTerceiraEtapa").hide();
        $("#divRowQuartaEtapa").hide();
        $("#divRowQuintaEtapa").hide();
        $("#divRowSextaEtapa").hide();
        $("#divRowSetimaEtapa").show();
    }

    console.log(proximaEtapa);
}

function AddTurno() {

    var cols =  "<div id='turno"+numeroTurno+"'><div class='col-md-2'><div class='col-md-12'><h4>Turno "+numeroTurno+"</h4></div><div class='col-md-12'><div class='form-group'><a href='#' onclick='RemoveTurno("+ numeroTurno +")' class='link-servico'><div class='removerTurn'>Remover</div></a></div></div></div>"+
                "<div class='col-md-10 text-center'><div class='row'><div class='col-md-15 no-padding-rg' style='text-align: right;padding-top: 30px;'><div class='form-group'><span>Inicio</span></div></div>"+
                "<div class='col-md-15 no-padding-rg'><div class='form-group'><label for='ini_dom"+numeroTurno+"'>Dom</label><input type='text' name='turno["+numeroTurno+"][ini_dom]' id='ini_dom"+numeroTurno+"' class='form-control'></div></div>"+
                "<div class='col-md-15 no-padding-rg'><div class='form-group'><label for='ini_seg"+numeroTurno+"'>Seg</label><input type='text' name='turno["+numeroTurno+"][ini_seg]' id='ini_seg"+numeroTurno+"' class='form-control'></div></div>"+
                "<div class='col-md-15 no-padding-rg'><div class='form-group'><label for='ini_ter"+numeroTurno+"'>Ter</label><input type='text' name='turno["+numeroTurno+"][ini_ter]' id='ini_ter"+numeroTurno+"' class='form-control'></div></div>"+
                "<div class='col-md-15 no-padding-rg'><div class='form-group'><label for='ini_qua"+numeroTurno+"'>Qua</label><input type='text' name='turno["+numeroTurno+"][ini_qua]' id='ini_qua"+numeroTurno+"' class='form-control'></div></div>"+
                "<div class='col-md-15 no-padding-rg'><div class='form-group'><label for='ini_qui"+numeroTurno+"'>Qui</label><input type='text' name='turno["+numeroTurno+"][ini_qui]' id='ini_qui"+numeroTurno+"' class='form-control'></div></div>"+
                "<div class='col-md-15 no-padding-rg'><div class='form-group'><label for='ini_sex"+numeroTurno+"'>Sex</label><input type='text' name='turno["+numeroTurno+"][ini_sex]' id='ini_sex"+numeroTurno+"' class='form-control'></div></div>"+
                "<div class='col-md-15 no-padding-rg'><div class='form-group'><label for='ini_sab"+numeroTurno+"'>Sab</label><input type='text' name='turno["+numeroTurno+"][ini_sab]' id='ini_sab"+numeroTurno+"' class='form-control'></div></div></div>"+
                "<div class='row'><div class='col-md-15 no-padding-rg' style='text-align: right;padding-top: 8px;'><div class='form-group'><span>Fim</span></div></div>"+
                "<div class='col-md-15 no-padding-rg'><div class='form-group'><input type='text' name='turno["+numeroTurno+"][fim_dom]' id='fim_dom"+numeroTurno+"' class='form-control'></div></div>"+
                "<div class='col-md-15 no-padding-rg'><div class='form-group'><input type='text' name='turno["+numeroTurno+"][fim_seg]' id='fim_seg"+numeroTurno+"' class='form-control'></div></div>"+
                "<div class='col-md-15 no-padding-rg'><div class='form-group'><input type='text' name='turno["+numeroTurno+"][fim_ter]' id='fim_ter"+numeroTurno+"' class='form-control'></div></div>"+
                "<div class='col-md-15 no-padding-rg'><div class='form-group'><input type='text' name='turno["+numeroTurno+"][fim_qua]' id='fim_qua"+numeroTurno+"' class='form-control'></div></div>"+
                "<div class='col-md-15 no-padding-rg'><div class='form-group'><input type='text' name='turno["+numeroTurno+"][fim_qui]' id='fim_qui"+numeroTurno+"' class='form-control'></div></div>"+
                "<div class='col-md-15 no-padding-rg'><div class='form-group'><input type='text' name='turno["+numeroTurno+"][fim_sex]' id='fim_sex"+numeroTurno+"' class='form-control'></div></div>"+
                "<div class='col-md-15 no-padding-rg'><div class='form-group'><input type='text' name='turno["+numeroTurno+"][fim_sab]' id='fim_sab"+numeroTurno+"' class='form-control'></div></div></div></div></div>";

    $("#turnos").append(cols);

    numeroTurno++;
}

function RemoveTurno(id) {
    var turno = $('#turno'+id);

    turno.fadeOut(400, function () {
        turno.remove();

    });

    numeroTurno--;

    console.log(numeroTurno);
}

function userTipo(type) {
    if(type === 1){
        $('#documentosPessoaFisica').show();
        $('#documentosPessoaJuridica').hide();
        $('#pessoa1').addClass('pessoaAct');
        if($('#pessoa2').hasClass('pessoaAct')){
            $('#pessoa2').removeClass('pessoaAct');
        }
    }else{
        $('#documentosPessoaJuridica').show();
        $('#documentosPessoaFisica').hide();
        $('#pessoa2').addClass('pessoaAct');
        if($('#pessoa1').hasClass('pessoaAct')){
            $('#pessoa1').removeClass('pessoaAct');
        }
    }
}