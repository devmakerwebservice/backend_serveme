$(function () {
    $("#uf_inscricao_estadual").change(function () {
        $("#inscricao_estadual").val('');
        $("#inscricao_estadual").attr("disabled", false);
        $("#inscricao_estadual").unmask();

        switch($(this).val()) {
            case "RS":
                $("#inscricao_estadual").mask("999-9999999", {placeholder: '###-#######'});
                break;
            case "SC":
                $("#inscricao_estadual").mask("999.999.999", {placeholder: '###.###.###'});
                break;
            case "PR":
                $("#inscricao_estadual").mask("99999999-99", {placeholder: '########-##'});
                break;
            case "SP":
                $("#inscricao_estadual").mask("999.999.999.999", {placeholder: '###.###.###.###'});
                break;
            case "MG":
                $("#inscricao_estadual").mask("999.999.999/9999", {placeholder: '###.###.###/####'});
                break;
            case "RJ":
                $("#inscricao_estadual").mask("99.999.99-9", {placeholder: '##.###.##-#'});
                break;
            case "ES":
                $("#inscricao_estadual").mask("999.999.99-9", {placeholder: '###.###.##-#'});
                break;
            case "BA":
                $("#inscricao_estadual").mask("999.999.99-9", {placeholder: '###.###.##-#'});
                break;
            case "SE":
                $("#inscricao_estadual").mask("999999999-9", {placeholder: '#########-#'});
                break;
            case "AL":
                $("#inscricao_estadual").mask("999999999", {placeholder: '#########'});
                break;
            case "PE":
                $("#inscricao_estadual").mask("99.9.999.9999999-9", {placeholder: '##.#.###.#######-#'});
                break;
            case "PB":
                $("#inscricao_estadual").mask("99999999-9", {placeholder: '########-#'});
                break;
            case "RN":
                $("#inscricao_estadual").mask("99.999.999-9", {placeholder: '##.###.###-#'});
                break;
            case "PI":
                $("#inscricao_estadual").mask("999999999", {placeholder: '#########'});
                break;
            case "MA":
                $("#inscricao_estadual").mask("999999999", {placeholder: '#########'});
                break;
            case "CE":
                $("#inscricao_estadual").mask("99999999-9", {placeholder: '########-#'});
                break;
            case "GO":
                $("#inscricao_estadual").mask("99.999.999-9", {placeholder: '##.###.###-#'});
                break;
            case "TO":
                $("#inscricao_estadual").mask("99999999999", {placeholder: '###########'});
                break;
            case "MT":
                $("#inscricao_estadual").mask("999999999", {placeholder: '#########'});
                break;
            case "MS":
                $("#inscricao_estadual").mask("999999999", {placeholder: '#########'});
                break;
            case "DF":
                $("#inscricao_estadual").mask("99999999999-99", {placeholder: '###########-##'});
                break;
            case "AM":
                $("#inscricao_estadual").mask("99.999.999-9", {placeholder: '##.###.###-#'});
                break;
            case "AC":
                $("#inscricao_estadual").mask("99.999.999/999-99", {placeholder: '##.###.###/###-##'});
                break;
            case "PA":
                $("#inscricao_estadual").mask("99-999999-9", {placeholder: '##-######-#'});
                break;
            case "RO":
                $("#inscricao_estadual").mask("999.99999-9", {placeholder: '###.#####-#'});
                break;
            case "RR":
                $("#inscricao_estadual").mask("99999999-9", {placeholder: '########-#'});
                break;
            case "AP":
                $("#inscricao_estadual").mask("999999999", {placeholder: '#########'});
                break;
        }
    });
});